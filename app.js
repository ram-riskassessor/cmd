let express = require("express");
let path = require("path");
let logger = require("morgan");
let cookieParser = require("cookie-parser");
let bodyParser = require("body-parser");
let device = require("express-device");
let fs = require("fs");
let app = express();
let cors = require("cors");
let config = require("./config/config.json")["server"];
let oauth = require("./config/oauthV2"),
  log = require("./controllers/log"),
  user = require("./controllers/users"),
  hasPermission = require("./config/hasPermission");
var index = require('./routes/index');
global.onlinehealth_app = app;

app.use(cors());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("/public", express.static(path.join(__dirname, "public")));
app.use("/public", (req, res) => res.status(404).end());
app.use('/', index);
app.use(device.capture());
global.image_url = config.image_url;

app.use(
  logger((tokens, req, res) =>
    [
      tokens.method(req, res),
      tokens.url(req, res),
      //       JSON.stringify(req.body, 0, 4),
      //       tokens.status(req, res),
      //       tokens.res(req, res, "content-length"),
      //       "-",
      //       tokens["response-time"](req, res),
      //       "ms"
    ].join(" ")
  )
);

/*
 * "express-load-routes" user for load routes
 */
require("express-load-routes")(app);

app.locals.site = {
  page: 10,
};

app.use(async function (req, res, next) {
  if (!req.controller) return next();

  if (req.actions) {
    let data = await hasPermission(req);
    if (!data.status) return res.send(data);

    req.user = {
      id: data.id,
      user_type: data.user_type,
      roleId: data.roleId,
      masterId: data.masterId,
    };
  } else {
    req.user = await user.getSessionUser(req);
  }

  try {
    await req
      .controller(req)
      //.then(data => console.log(data))
      .then((data) => res.send(data))
      .catch(console.log);
    // .catch(() =>
    //   res.send({
    //     status: false,
    //     error: true,
    //     error_description: "Internal Error"
    //   })
    // );
  } catch (err) {
    res.send(log(req, err));
  }
});

app.use(oauth.errorHandler());

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  //console.log(err);

  // render the error page
  res.status(err.status || 500);
  res.send({
    status: false,
    error: true,
    error_description: "Internal Error",
    url: true,
  });
});

tmpDir = "/tmp/onlinehealth/";
try {
  fs.accessSync("public/uploads");
} catch (err) {
  fs.mkdirSync("public/uploads");
}
try {
  fs.accessSync(tmpDir);
} catch (err) {
  fs.mkdirSync(tmpDir);
}

Number.prototype.toHHMMSS = function () {
  var sec_num = parseInt(this, 10); // don't forget the second param
  var hours = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - hours * 3600) / 60);
  var seconds = sec_num - hours * 3600 - minutes * 60;

  if (hours < 10) {
    hours = "0" + hours;
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  if (seconds < 10) {
    seconds = "0" + seconds;
  }
  return hours + ":" + minutes + ":" + seconds;
};

module.exports = app;
